﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Enquete.Util
{
    public class Util : DefaultContractResolver
    {

		private HashSet<string> _props;
		private bool _aceitar;

		public Util(IEnumerable<string> propNames, bool aceitar)
		{
			_props = new HashSet<string>(propNames);
			_aceitar = aceitar;
		}

		protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
		{
			JsonProperty property = base.CreateProperty(member, memberSerialization);

			

			if (_props.Contains(property.PropertyName) && _aceitar == true)
			{
				property.ShouldSerialize = (x) => { return true; };
			}
			else if(!_props.Contains(property.PropertyName) && _aceitar == false)
            {
				property.ShouldSerialize = (x) => { return true; };
			}
			else
            {
				property.ShouldSerialize = (x) => { return false; };
			}

			return property;
		}
	}
}
