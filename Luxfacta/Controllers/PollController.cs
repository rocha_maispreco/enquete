﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.WebEncoders.Testing;
using Newtonsoft.Json;
//using Newtonsoft.Json;
using Enquete.Elementos;
using Enquete.Elementos.ElementosDTO;
using Enquete.Interfaces;
namespace Enquete.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class PollController : ControllerBase
    {
        private readonly ILogger<PollController> _logger;
        private readonly IPollService _PollService;

        public PollController(IPollService PollService, ILogger<PollController> logger)
        {
            _PollService = PollService;
            _logger = logger;
        }

        [HttpGet]        
     
         public async Task<IActionResult> GetTodosAsync()
        {   
            var retornoTodos = await _PollService.ListarAsync();
            if (retornoTodos == null)
                return NotFound();

            string retornoJson = JsonConvert.SerializeObject(retornoTodos, new JsonSerializerSettings()
            { ContractResolver = new Util.Util(new[] { "CountView", "FK_POLL_ID", "ID" }, false) });
            return Ok(retornoJson);
        }
        
        [Route("{id}")]
        [HttpGet]
        public async Task<IActionResult> BuscarPorIdAsync(int id)
        {
            
            var retornoPorId = await  _PollService.BuscarPorIdAsync(id);
            if (retornoPorId == null)
                return NotFound();
            string retornoJson = JsonConvert.SerializeObject(retornoPorId, new JsonSerializerSettings()
            { ContractResolver = new Util.Util(new[] { "CountView", "FK_POLL_ID", "ID", "qtd_Votos" }, false) });
            return Ok(retornoJson);

            
        }
        [HttpPost]
        public async Task<string> AdicionarAsync(OpcoesDTO opcoesDTO)
        {
            Poll poll = new OpcoesDTO().ConvertParaDTO(opcoesDTO);

            var retornoIdAdicionado =   await _PollService.AdicionarAsync(poll);
            return JsonConvert.SerializeObject(retornoIdAdicionado, new JsonSerializerSettings()
            { ContractResolver = new Util.Util(new[] { "poll_id" }, true) });
        }
        [HttpPut]
        public void AtualizarAsync(Poll Poll)
        {
             _PollService.AtualizarAsync(Poll);            
        }
        [HttpDelete]
        public void RemoverAsync(Poll Poll)
        {
            _PollService.RemoverAsync(Poll);
        }

        [Route("{id}/vote")]        
        [HttpPost]
        public async Task<IActionResult> Vote(int id,VotoDTO votoDTO)
        {
            votoDTO.id_Poll = id;         

            var retornoPorId = await _PollService.inserirVoto(votoDTO);
            if (retornoPorId == 0)
                return NotFound();
     
            return Ok();
        }

        [Route("{id}/stats")]
        [HttpGet]
        public async Task<IActionResult> Stats(int id)
        {

            var retornoPorId = await _PollService.BuscarStatus(id);
            if (retornoPorId == null)
                return NotFound();
            string retornoJson = JsonConvert.SerializeObject(retornoPorId, new JsonSerializerSettings()
            { ContractResolver = new Util.Util(new[] {"poll_id","poll_description","FK_POLL_ID", "ID", "option_description" }, false) });
            return Ok(retornoJson);
        }
    }
}
