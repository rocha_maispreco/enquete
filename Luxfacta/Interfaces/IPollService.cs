﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Enquete.Elementos;

namespace Enquete.Interfaces
{
    public interface IPollService
    {
        Task<IEnumerable<Poll>> ListarAsync();
        Task<Poll> BuscarPorIdAsync(int id);
        Task<Poll> AdicionarAsync(Poll Poll);
        void AtualizarAsync(Poll Poll);
        void RemoverAsync(Poll Poll);
        Task<int> inserirVoto(VotoDTO votoDTO);
        Task<Poll> BuscarStatus(int id);

    }
}
