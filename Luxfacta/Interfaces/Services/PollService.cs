﻿using Enquete.Elementos;
using Enquete.Interfaces;
using Enquete.Persistencia.Repositorios;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enquete.Interfaces.Services
{
    public class PollService : IPollService
    {

        private readonly IPollRepositorio _PollRepositorio;
        public PollService(IPollRepositorio PollRepositorio)
        {
            _PollRepositorio = PollRepositorio;
        }
        public Task<IEnumerable<Poll>> ListarAsync()
        {
           return _PollRepositorio.ListarTodosAsync();

        }
        public Task<Poll> BuscarPorIdAsync(int id)
        {
            return _PollRepositorio.BuscarPorIdAsync(id);

        }
        public async Task<Poll> AdicionarAsync(Poll Poll)
        {
            return await _PollRepositorio.AdicionarAsync(Poll);

        }
        public void AtualizarAsync(Poll Poll)
        {
            _PollRepositorio.Atualizar(Poll);

        }
        public void RemoverAsync(Poll Poll)
        {
            _PollRepositorio.Remover(Poll);

        }
        public async Task<int> inserirVoto(VotoDTO votoDTO)
        {
           return await _PollRepositorio.inserirVoto(votoDTO);
        }
        public Task<Poll> BuscarStatus(int id)
        {
            return _PollRepositorio.BuscarStatus(id);

        }
    }
}
