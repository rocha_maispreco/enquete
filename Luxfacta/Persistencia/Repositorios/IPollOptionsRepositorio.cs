﻿using Enquete.Elementos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enquete.Persistencia.Repositorios
{
    public interface IPollOptionsRepositorio
    {
        Task<Poll> AdicionarAsync(PollOptions PollOptions);      
    }
}
