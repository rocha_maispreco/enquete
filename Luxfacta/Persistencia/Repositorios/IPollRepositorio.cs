﻿using Enquete.Elementos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enquete.Persistencia.Repositorios
{
    public interface IPollRepositorio
    {
        Task<IEnumerable<Poll>> ListarTodosAsync();
        Task<Poll> AdicionarAsync(Poll Poll);
        Task<Poll> BuscarPorIdAsync(int id);
        Task<int> Atualizar(Poll Poll);
        int Remover(Poll Poll);
        Task<int> inserirVoto(VotoDTO votoDTO);
        Task<Poll> BuscarStatus(int id);
        
    }
}
