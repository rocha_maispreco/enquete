﻿using Enquete.Persistencia.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enquete.Persistencia.Base
{
    public class BaseContexto
    {
        private readonly AppDbContext _context;
        public BaseContexto(AppDbContext appContext)
        {
            _context = appContext;
        }
    }
}
