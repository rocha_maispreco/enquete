﻿using Enquete.Elementos;
using Enquete.Persistencia.Contexto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Enquete.Persistencia.Repositorios;


namespace Enquete.Persistencia.Base
{
    public class PollRepositorio : BaseContexto, IPollRepositorio
    {
        private readonly AppDbContext _context;
        public PollRepositorio(AppDbContext context) : base(context)
        {
            _context = context;
        }
        public async Task<IEnumerable<Poll>> ListarTodosAsync()
        {
            try
            {
                return await _context.Polls.ToListAsync();
            }
            catch (Exception)
            {

                throw new Exception("Erro ao listar todos os votos!");
            }

        }
        public async Task<Poll> AdicionarAsync(Poll poll)
        {
            try
            {
                int idOpcao = 1;
                foreach (PollOptions item in poll.Options)
                {
                    item.option_id = idOpcao;
                    item.qtd_Votos = 0;
                    idOpcao++;
                }

                int linhasAdicionadas;
                if (poll.poll_id == 0)
                {
                    _context.Polls.Add(poll);
                    linhasAdicionadas = await _context.SaveChangesAsync();
                }
                else
                {
                    return null;
                }
                
                if (linhasAdicionadas == 0)                
                    return null;
                
                
                //linhasAdicionadas = await _context.SaveChangesAsync();
                //if (linhasAdicionadas == 0)
                //{
                //    _context.Polls.Remove(poll);
                //    await _context.SaveChangesAsync();
                //    return null;
                //}

                return poll;
            }
            catch (Exception)
            {
                _context.Remove(poll);
                await _context.SaveChangesAsync();
                throw new Exception("Erro ao adicionar nova enquete!");
            }
        }
        public async Task<Poll> BuscarPorIdAsync(int id)
        {
            try
            {
                var returnPoll = await _context.Polls.FindAsync(id);
                if (returnPoll == null)
                    return null;
                returnPoll.CountView += 1;

                int retornoIncrementoViews = incrementarViews(returnPoll).Result;
                if (retornoIncrementoViews < 1)
                {
                    //Salvar em log - Views não incrementado
                }
                return returnPoll;
            }
            catch (Exception)
            {

                throw new Exception("Erro ao buscar por ID.");
            }
        }

        public async Task<Poll> BuscarStatus(int id)
        {
            try
            {
                var returnPoll = await _context.Polls.FindAsync(id);
                if (returnPoll == null)
                    return null;
               
                return returnPoll;
            }
            catch (Exception)
            {
                throw new Exception("Erro ao buscar por ID.");
            }
        }
        private async Task<int> incrementarViews(Poll poll)
        {
            _context.Polls.Update(poll);
            return await _context.SaveChangesAsync();

        }
        public int Remover(Poll Poll)
        {
            try
            {


                var PollDeletado = _context.Polls.Remove(Poll);
                if (PollDeletado.State == EntityState.Deleted)
                    return 1;
            }
            catch (Exception)
            {

                throw new Exception("Erro ao remover enquete.");

            }
            return 0;
        }
        public async Task<int> Atualizar(Poll Poll)
        {
            try
            {
                _context.Polls.Update(Poll);
                return await _context.SaveChangesAsync();
            }
            catch (Exception)
            {

                throw new Exception("Erro ao atualizar enquete.");
            }
        }
        public async Task<int> inserirVoto(VotoDTO votoDTO)
        {
            try
            {


                var returnPoll = await _context.Polls.FindAsync(votoDTO.id_Poll);
                if (returnPoll == null)
                    return 0;
                PollOptions opcao = returnPoll.Options.FirstOrDefault(t => t.option_id == votoDTO.option_id);
                if (opcao == null)
                    return 0;
                opcao.qtd_Votos += 1;
                _context.pollOptions.Update(opcao);
                int linhasModificadas = await _context.SaveChangesAsync();

                if (linhasModificadas > 0)
                    return 1;
            }
            catch (Exception)
            {                
                throw new Exception("Erro ao registrar voto.");
            }

            return 0;
        }
    }
}
