﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Enquete.Elementos;

namespace Enquete.Mapeador
{
    public class PollMap : IEntityTypeConfiguration<Poll>
    {


        public void Configure(EntityTypeBuilder<Poll> builder)
        {
            builder.ToTable("POLL");

            builder.HasKey(x => x.poll_id);
            builder.Property(x => x.poll_id)
            .HasColumnName("POLL_ID");
            // .IsRequired();

            
            builder.Property(x => x.poll_description)
            .HasColumnName("POLL_DESCRIPTION")
            .IsRequired();

            builder.HasMany(x => x.Options);
            //builder.Property(x => x.Opcoes)              
            //.HasColumnName("ID_OPTION")
            //.IsRequired();

            builder.Property(x => x.CountView)
          .HasColumnName("COUNT_VIEW")
          .IsRequired();
        }
    }
}
