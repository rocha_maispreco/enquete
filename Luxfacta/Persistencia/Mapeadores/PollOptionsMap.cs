﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Enquete.Elementos;

namespace Enquete.Mapeador
{
    public class PollOptionsMap : IEntityTypeConfiguration<PollOptions>
    {
        public void Configure(EntityTypeBuilder<PollOptions> builder)
        {
            builder.ToTable("OPTIONS");

            builder.HasKey(x => x.ID);
            builder.Property(x => x.FK_POLL_ID)
        .HasColumnName("FK_POLL_ID");
            // .IsRequired();

            builder.Property(x => x.option_id)
            .HasColumnName("OPTION_ID")
            .IsRequired();

            builder.Property(x => x.option_description)
            .HasColumnName("OPTION_DESCRIPTION")
            .IsRequired();

            builder.Property(x => x.qtd_Votos)
                .HasColumnName("QTD_VOTOS")
                .IsRequired();
        }
    }
}
