﻿using Microsoft.EntityFrameworkCore;
using Enquete.Elementos;
using Enquete.Mapeador;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enquete.Persistencia.Contexto
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }


        public DbSet<Poll> Polls { get; set; }
        public DbSet<PollOptions> pollOptions{get;set;}
      
        protected override void OnModelCreating(ModelBuilder builder)
        {
          
            builder.ApplyConfiguration(new PollMap());
            builder.ApplyConfiguration(new PollOptionsMap());
           

        }
    }
}
