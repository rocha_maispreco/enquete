using Castle.Components.DictionaryAdapter;
using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json.Serialization;

namespace Enquete.Elementos
{
	public class PollOptions
	{
		
		[Key("ID")]
		public int ID { get; set; }		
		public int FK_POLL_ID { get; set; }
		public int option_id { get; set; }
		public string option_description { get; set; }
		public int qtd_Votos { get; set; }
	}
}
