﻿using Microsoft.AspNetCore.Authorization.Policy;
using Enquete.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enquete.Elementos.ElementosDTO
{
    public class OpcoesDTO
    {
       public string poll_description { get; set; }
       public List<string> options { get; set; }

        public Poll ConvertParaDTO(OpcoesDTO opcoesDTO)
        {
            Poll pollConvertido = new Poll();
            pollConvertido.poll_description = opcoesDTO.poll_description;
            pollConvertido.Options = new List<PollOptions>();
            
            
            foreach (string item in opcoesDTO.options)
            {
               
                pollConvertido.Options.Add(new PollOptions { option_description = item});
                   
            }
            return pollConvertido;
        }
    }
}
