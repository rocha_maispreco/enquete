﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Enquete.Elementos
{
    public class VotoDTO
    {
        public int id_Poll { get; set; }
        public int option_id { get; set; }
    }
}
