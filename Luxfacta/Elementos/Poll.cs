using Microsoft.EntityFrameworkCore.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Policy;
using System.Text.Json.Serialization;

namespace Enquete.Elementos
{
	public class Poll
	{


        private ICollection<PollOptions> _options;
        public Poll()
        {

        }
        private Poll(ILazyLoader lazyLoader)
        {
        	LazyLoader = lazyLoader;
        }
        private ILazyLoader LazyLoader { get; set; }


        public int poll_id { get; set; }
        public string poll_description { get; set; }


        public int CountView { get; set; }
        [ForeignKey("FK_POLL_ID")]
        public ICollection<PollOptions> Options
        {
        	get => LazyLoader.Load(this, ref _options);
        	set => _options = value;
        }
    }
}
